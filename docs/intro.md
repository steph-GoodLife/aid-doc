---
sidebar_position: 1
---

# Tutorial Intro

Docusaurus permet de créer toutes la documentaion nécessaires et peut être régulièrement mise à jour si besoin.

## Getting Started

Dans la sidebar juste à gauche se trouve le menu qui vous guidera à travers la documentation de votre site.

Dans chacune des pages de la doc, une sidebar, sur la droite, regroupe les différents titres de la page qui vous permettront de naviguer plus facilement à l'intérieur en cliquant dessus.

En bas de page de chaque doc, vous pouvez passer à la suite ou revenir en arrière en cliquant sur <code>Next</code> ou <code>Previous</code>

:::success &#128512;
Bonne lecture !

Si quelque chose n'est pas clair, contactez directement Stéphane <code>webprod@cf2m.be</code>
:::




