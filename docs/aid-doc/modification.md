---
sidebar_position: 2
---

# Modification de pages avec Elementor

### Modifier les sections et pages avec Elementor

Toutes les sections et pages seront éditer/modifier avec l'éditeur Elementor qui est un plugin installé à cet effet et qui vient remplacer l'éditeur natif de WordPress.

Cet editeur majoritairement répandu dans la communauté WP permet plus liberté dans l'édition de contenu et sa mise en forme.


### L'éditeur Elementor

**Avant de commencer**

:::note remarque !

Toutes les modifications sur les pages et sections devront être effectuées via <code>Elementor</code>
:::

**Comment accéder à l'éditeur Elementor**

Lorsque vous choisissez une section ou une page dans l'Admin, vous devez survoler avec la souris le titre de la section/page que vous souhaitez modifier, afin de faire apparaitre les choix de modification.

En bout de ligne de ces choix, vous verrez <code>modifier avec Elementor</code> , puis cliquez dessus.

<img src={require('@site/static/img/elementor-1.jpg').default} />

### L'éditeur Elementor sur votre page

Elementor s'ouvre sur la gauche de l'écran et fourni en premier lieu une serie de choix de tous ce que vous pouvez ajouter.

Il vous suffit de faire défiler les élements vers le bas pour découvrir d'autres options.

Elementor fonctionne par <code>Drag and Drop</code> - glissé-déposé vers la section que vous souhaitez éditer.

<img src={require('@site/static/img/elementor-2.jpg').default} />




### Modifier les éléments

Une fois la page ouverte par Elementor, vous pourrez modifier un élement simplement en cliquant dessus.
Que ce soit du texte, une image, un titre, ...

Lorsque vous passerez la souris sur l'élément, une colonne encadrant cet élément apparaitra.
Il s'agit de la zone où l'élément est contenu et positionné sur votre page.

Elementor ouvre ensuite l'éditeur avec l'élément à modifier.

Modifier l'élément directement dans cet éditeur. Et ajoutez lui le style que vous souhaitez grâce aux options proposées en dessous et dans le menu au dessus : <code>Contenu</code> - <code>Style</code> - <code>Avancé</code>

- <code>Style</code> : couleur de texte - Typographie (police, taille, ...)

- <code>Avancé</code> : marges extérieures - marges intérieures - effets de mouvement - bordures - etc...

<img src={require('@site/static/img/elementor-3.jpg').default} />

***N'oubliez pas de <code>Mettre à jour</code> après avoir terminé vos modifications***

<img src={require('@site/static/img/elementor-6.jpg').default} />


### copier un bloc

Vous pouvez simplement vouloir copier un bloc existant afin de garder la même mise en forme du contenu et simplement modifier les textes, images, titres,...

Dans ce cas, il vous simplement de le copier-coller.

Pour copier-coller, il vous suffit de survoler le bloc que vous souhaitez copier avec votre souris.
En haut à gauche du bloc, un carré noir apparait - faites un clic droit avec votre souris sur ce petit carré noir.

- **Sélectionnez <code>copié</code>**

<img src={require('@site/static/img/elementor-4.jpg').default} />

- **Placé vous sur le bloc dans lequel vous souhaitez le coller. Et faites un clic droit -> collé**

<img src={require('@site/static/img/elementor-5.jpg').default} />

:::note
***N'oubliez pas de <code>Mettre à jour</code> après avoir terminé vos modifications***
:::

### Revenir au tableau de bord

Pour revenir au tableau de bord en 3 clics :

**- cliquez sur le burger en haut à gauche de Elementor**

<img src={require('@site/static/img/elementor-7.jpg').default} />


**- cliquez sur <code>Revenir à l'éditeur Wordpress</code>**

<img src={require('@site/static/img/elementor-8.jpg').default} />

**- cliquez sur le carré noir de WordPress**

<img src={require('@site/static/img/elementor-9.jpg').default} />

Vous serez de retour sur le tableau de bord.


:::caution attention !
l'entête et le footer ne sont pas modifiable par Elementor !
:::

:::success &#128077;
Si tout est OK pour vous ici, passez à la suite
:::