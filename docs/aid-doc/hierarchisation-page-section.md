---
sidebar_position: 1
---

# Hiérachisation des pages et des sections

### Admin -> sidebar de gauche

Toutes vos pages et sections se retrouvent dans la sidebar de gauche dans votre Admin.

Le site utilise des pages et des sections pour afficher le contenu.

Dans notre cas, nous utilisons principalement des SECTIONS ou CPT pour Custom Post Type.

La seule page utilisée se trouve dans la partie <code>Pages</code>, il s'agit de la page <code>Home</code>, qui est la page d'accueil du site.

Dans notre exemple ci-dessous, <code>Projets</code> est une section (CPT) qui regroupe les pages de sous-menu de sa section.

<img src={require('@site/static/img/doc-1.PNG').default} />

Ainsi, tout ce que vous souhaitez modifier en terme de contenu est à modifier dans ces parties de l'Admin, en selectionnant le nom de la page ou section à modifier.

*Titres, Textes, Images, etc...*

**Les nom de ces <code>sections</code>sont : **

> Membres

> Projets

> Offres d'emploi

> Publications

> Contact et liens

>Fiches des membres

### Le menu

Tous les titres de ces sections et pages sont présents dans le menu du site

Prenant l'exemple de l'onglet <code>PROJETS</code> :

<img src={require('@site/static/img/doc-3.jpg').default} />

On voit ci-dessus que <code>PROJETS</code> est le nom d'une section (voir image 1)

Et le sous-menu qui s'affiche est l'ensemble des pages présentes dans cette section <code>PROJETS</code>

### Modification d'une page dans une section

Sur ce principe vu ci-dessus, lorsque vous souhaitez modifier/éditer une section, il vous sufit de la selectionner ( dans notre exemple, il s'agit de <code>PROJETS</code> ) et de selectionner la page que vous souhaitez modifier

<img src={require('@site/static/img/doc-2.PNG').default} />

Repérez le titre dans le menu - selectionnez le dans l'admin - selectionnez la page que vous voulez éditer

### la page d'accueil du site

Comme expliqué plus haut, le site utilise principalement des sections qui permet de mieux diviser chaque partie et donc permettre une meilleure lisibilité de votre admin.

A une exception... la page d'accueil du site: <code>Home</code>

Cette page d'accueil se trouve dans le partie <code>Pages</code> de votre admin

<img src={require('@site/static/img/doc-4.jpg').default} />

:::caution attention !
La page Contact se modifie depuis la section <code>Contact et liens</code>

Contact n'est pas une page de la partie <code>Pages</code> !
:::

### La section <code>Fiche des membres</code>

Cette section regroupe toutes les fiches des membres AID où figure toutes leurs infos.

Elle n'est pas présente dans le menu du site. Et est accéssible via le menu <code>Membres</code> -> <code>Recherche</code> -> choisir sur la carte ou dans la liste en dessous de la carte.


:::success &#128077;

Si tout est OK pour vous ici, passez à la suite <code>Modification sur les pages</code>.
Cliquez sur Next ci-dessous.

:::




