---
sidebar_position: 3
---

# Tous les codes

> Les codes couleurs

> Les typos

> les bordures

> les marges

### Les couleurs

Le vert du site : #84C340 / rgb(132,195,64)

Le orange du site: #FF521E / rgb(255,82,30)


### Les typos

LES TITRES et SOUS-TITRES : Architeste Daughter

LES TEXTES : Archivo

*Lorsque la typo <code>Archivo</code> n'est pas disponible dans un plugin, elle sera remplacé par <code>Arial</code>*

### Les bordures

sur les pages du site, le contenu est entouré par une bordure.

:::caution attention !
La bordure s'applique sur la colonne du contenu.

Pas sur directement sur le contenu (texte, image, ...)
:::

- *Pour modifier la colonne, passage avec la souris sur le contenu et clic sur le carré noir dans le coin supérieur à gauche qui s'affiche*
- *dans l'éditeur Elementor, clic sur style->bordure*

Couleur de la bordure: #84C340 / rgb(132,195,64)

Largeur de la bordure: 2px

Rayon de bordure: 5px

activer <code>Ombre de boite</code> en cliquant sur le crayon

<img src={require('@site/static/img/elementor-10.jpg').default} />


### Les marges

Les colonnes et leur contenu dans les pages, disposées une en dessous de l'autre, à la suite, sont séparées par une <code>marge supérieur</code>

**Espace de la marge : 20px**

- *Pour modifier la colonne, passage avec la souris sur le contenu et clic sur le carré noir dans le coin supérieur à gauche qui s'affiche*
- *dans l'éditeur Elementor, clic sur Avancé->Avancé->Marge->HAUT*
- *! décoché le maillot grisé avant de modifier la marge !*

<img src={require('@site/static/img/elementor-11.jpg').default} />

