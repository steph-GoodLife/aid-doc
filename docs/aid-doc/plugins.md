---
sidebar_position: 4
---

# Liste des plugins

### Tous les plugins sont regroupés dans la partie Admin du site

**Newsletter**

> Newsletter

**Formulaire de contact**

> WPForms

**Les cookies**

> Cookies

**Bouton "Lire plus"**

>Read More

**Ajout de catégorie**

> CPT UI

**Filtre dans page Recherche/carte**

> Search & Filter

**Traduction**

> Weglot

**Slide Accueil**

> Smart Slider

**Carte**

> Leaflet

