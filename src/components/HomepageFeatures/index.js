import React from 'react';
import clsx from 'clsx';
import styles from './styles.module.css';

const FeatureList = [
  {
    title: 'Architecture des pages',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        Où retrouver vos pages et sections regroupées dans le menu.
        Et comment sont-elles hiérarchisées.
        A quoi correspond chaque section (CPT : Custom Post Type) et qu'est ce qu'on y retrouve.
      </>
    ),
  },
  {
    title: 'modifications sur les pages',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        Comment modifier le contenu des pages.Utilisation de <code>Elementor</code>.

      </>
    ),
  },
  {
    title: 'Liste des plugins',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        Tous les plugins que le site utilise. Où les trouver et comment modifier leur contenu.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
